TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += ..\src\columns_sms.c \
    ../src/bank1.c \
    ../src/board_tm.c \
    ../src/number_manipulation.c \
    ../src/utils.c \
    ../src/state_machine.c \
    ../src/main_menu.c \
    ../src/board_tm.c \
    ../src/palette.c \
    ../src/game.c \
    ../src/game_matrix.c \
    ../src/gems_block.c \
    ../src/array_utils.c \
    ../src/gem_tiles.c \
    ../src/score_controller.c \
    ../src/bank3.c \
    ../src/presentation_screen.c \
    ../src/gemitas.c

HEADERS += \
    ../src/bank1.h \
    ../src/board_tm.h \
    ../src/number_manipulation.h \
    ../src/PSGlib.h \
    ../src/SMSlib.h \
    ../src/utils.h \
    ../src/state_machine.h \
    ../src/main_menu.h \
    ../src/board_tm.h \
    ../src/palette.h \
    ../src/types.h \
    ../src/constants.h \
    ../src/game.h \
    ../src/game_matrix.h \
    ../src/gems_block.h \
    ../src/array_utils.h \
    ../src/gem_tiles.h \
    ../src/score_controller.h \
    ../src/bank3.h \
    ../src/presentation_screen.h
