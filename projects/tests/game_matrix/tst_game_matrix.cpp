#include <QString>
#include <QtTest>

extern "C"
{
#include "../../../src/game_matrix.h"
}

class GameMatrixTests : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void noColumnOrRowSet();
    void setAndCheck();
};

void GameMatrixTests::noColumnOrRowSet()
{
    Byte columns = 0;
    unsigned int rows = 0;

    for(std::size_t i = 0; i < sizeof(columns) * 8; ++ i)
    {
        QVERIFY2(!GameMatrix_isColumnSet(columns, i), (QString{"Column %1 is set"}.arg(i)).toUtf8());
    }

    for(std::size_t i = 0; i < sizeof(rows) * 8; ++ i)
    {
        QVERIFY2(!GameMatrix_isRowSet(rows, i), (QString{"Row %1 is set"}.arg(i)).toUtf8());
    }
}

void GameMatrixTests::setAndCheck()
{
    Byte columns = 0;

    for(std::size_t i = 0; i < sizeof(columns) * 8; ++ i)
    {
        columns = 0;
        GameMatrix_setColumn(columns, i);
        QVERIFY2(GameMatrix_isColumnSet(columns, i), (QString{"Column %1 is not set"}.arg(i)).toUtf8());
    }

    unsigned int rows = 0;

    for(std::size_t i = 0; i < sizeof(rows) * 8; ++ i)
    {
        rows = 0;
        GameMatrix_setColumn(rows, i);
        QVERIFY2(GameMatrix_isColumnSet(rows, i), (QString{"Row %1 is not set"}.arg(i)).toUtf8());
    }
}

QTEST_APPLESS_MAIN(GameMatrixTests)

#include "tst_game_matrix.moc"
