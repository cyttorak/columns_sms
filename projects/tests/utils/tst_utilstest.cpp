#include <QString>
#include <QtTest>
#include <array>

#include <QDebug>

extern "C"
{
#include "../../../src/array_utils.h"
}

class UtilsTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void compactEmptyArray();
    void compactArrayWithOneHole();
    void compactArrayWithVariousHoles();

    void compactEmptyReverseArray();
    void compactReverseArrayWithOneHole();
    void compactReverseArrayWithVariousHoles();

    void rotateArrayToRight();
    void rotateArrayToLeft();
};

void UtilsTest::compactEmptyArray()
{
    const Byte EmptyValue = 20;
    std::array<Byte, 10> array = {EmptyValue, EmptyValue, EmptyValue, EmptyValue, EmptyValue, EmptyValue, EmptyValue, EmptyValue, EmptyValue, EmptyValue};
    const auto referenceArray = array;

    AU_compactArray(array.data(), array.size(), EmptyValue);

    QVERIFY2(array == referenceArray, "Compacted array is not correct");
}

void UtilsTest::compactArrayWithOneHole()
{
    const Byte EmptyValue = 20;
    std::array<Byte, 10> array = {0, EmptyValue, 1, 2, 3, 4, 5, 6, 7, 8};
    const std::array<Byte, 10> referenceArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, EmptyValue};

    AU_compactArray(array.data(), array.size(), EmptyValue);

    QVERIFY2(array == referenceArray, "Compacted array is not correct");
}

void UtilsTest::compactArrayWithVariousHoles()
{
    const Byte EmptyValue = 20;
    std::array<Byte, 10> array = {0, EmptyValue, 1, 2, EmptyValue, EmptyValue, EmptyValue, 3, 4, 5};
    const std::array<Byte, 10> referenceArray = {0, 1, 2, 3, 4, 5, EmptyValue, EmptyValue, EmptyValue, EmptyValue};

    AU_compactArray(array.data(), array.size(), EmptyValue);

    QVERIFY2(array == referenceArray, "Compacted array is not correct");
}

void UtilsTest::compactEmptyReverseArray()
{
    const Byte EmptyValue = 20;
    std::array<Byte, 10> array = {EmptyValue, EmptyValue, EmptyValue, EmptyValue, EmptyValue, EmptyValue, EmptyValue, EmptyValue, EmptyValue, EmptyValue};
    const auto referenceArray = array;

    AU_compactReverseArray(array.data(), array.size(), EmptyValue);

    QVERIFY2(array == referenceArray, "Compacted array is not correct");
}

void UtilsTest::compactReverseArrayWithOneHole()
{
    const Byte EmptyValue = 20;
    std::array<Byte, 10> array = {0, EmptyValue, 1, 2, 3, 4, 5, 6, 7, 8};
    const std::array<Byte, 10> referenceArray = {EmptyValue, 0, 1, 2, 3, 4, 5, 6, 7, 8};

    AU_compactReverseArray(array.data(), array.size(), EmptyValue);

    QVERIFY2(array == referenceArray, "Compacted array is not correct");
}

void UtilsTest::compactReverseArrayWithVariousHoles()
{
    const Byte EmptyValue = 20;
    std::array<Byte, 10> array = {0, EmptyValue, 1, 2, EmptyValue, EmptyValue, EmptyValue, 3, 4, 5};
    const std::array<Byte, 10> referenceArray = {EmptyValue, EmptyValue, EmptyValue, EmptyValue, 0, 1, 2, 3, 4, 5};

    AU_compactReverseArray(array.data(), array.size(), EmptyValue);

    QVERIFY2(array == referenceArray, "Compacted array is not correct");
}

void UtilsTest::rotateArrayToRight()
{
    std::array<Byte, 5> array = {0, 1, 2, 3, 4};
    const std::array<Byte, 5> referenceArray = {4, 0, 1, 2, 3};

    AU_rotateArrayToRight(array.data(), array.size());

    QVERIFY2(array == referenceArray, "Rotation to right is not correct");
}

void UtilsTest::rotateArrayToLeft()
{
    std::array<Byte, 5> array = {0, 1, 2, 3, 4};
    const std::array<Byte, 5> referenceArray = {1, 2, 3, 4, 0};

    AU_rotateArrayToLeft(array.data(), array.size());

    QVERIFY2(array == referenceArray, "Rotation to left is not correct");
}



QTEST_APPLESS_MAIN(UtilsTest)

#include "tst_utilstest.moc"
