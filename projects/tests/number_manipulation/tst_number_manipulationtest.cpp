#include <QString>
#include <QtTest>

extern "C"
{
#include "../../../src/number_manipulation.h"
}

class NumberManipulationTests : public QObject
{
    Q_OBJECT

public:

private Q_SLOTS:
    void lastDigitIndexCharShouldReturn0();
    void lastDigitIndexCharWith123ShouldReturn1Then0();

    void lastDigitIndexUIntShouldReturn0();
    void lastDigitIndexUIntWith12345ShouldReturnCorrectIndicesAndNumbers();

    void lastDigitIndexULongShouldReturn0();
    void lastDigitIndexULongWith1234567890ShouldReturnCorrectIndicesAndNumbers();
};

void NumberManipulationTests::lastDigitIndexCharShouldReturn0()
{
    char number = 0;
    Byte index;
    char newNumber = NM_getLastDigitIndexChar(number, &index);
    QVERIFY2(newNumber == 0, "Returned value is not 0 when number = 0");
    QVERIFY2(index == 0, "Index is not 0");
}

void NumberManipulationTests::lastDigitIndexCharWith123ShouldReturn1Then0()
{
    char number = 123;
    Byte index;

    number = NM_getLastDigitIndexChar(number, &index);
    QVERIFY2(number == 12, "Returned value is not 12 when number = 123");
    QVERIFY2(index == 3, "Index is not 3");

    number = NM_getLastDigitIndexChar(number, &index);
    QVERIFY2(number == 1, "Returned value is not 1 when number = 12");
    QVERIFY2(index == 2, "Index is not 2 when number = 12");

    number = NM_getLastDigitIndexChar(number, &index);
    QVERIFY2(number == 0, "Returned value is not 0 when number = 1");
    QVERIFY2(index == 1, "Index is not 1 when number = 1");

    number = NM_getLastDigitIndexChar(number, &index);
    QVERIFY2(number == 0, "Returned value is not 0 when number = 0");
    QVERIFY2(index == 0, "Index is not 0 when number = 0");
}

void NumberManipulationTests::lastDigitIndexUIntShouldReturn0()
{
    unsigned int number = 0;
    Byte index;
    unsigned int newNumber = NM_getLastDigitIndexUInt(number, &index);
    QVERIFY2(newNumber == 0, "Returned value is not 0 when number = 0");
    QVERIFY2(index == 0, "Index is not 0");
}

void NumberManipulationTests::lastDigitIndexUIntWith12345ShouldReturnCorrectIndicesAndNumbers()
{
    unsigned int number = 12345;
    Byte index;

    number = NM_getLastDigitIndexUInt(number, &index);
    QVERIFY2(number == 1234, "Returned value is not 1234 when number = 12345");
    QVERIFY2(index == 5, "Index is not 5");

    number = NM_getLastDigitIndexUInt(number, &index);
    QVERIFY2(number == 123, "Returned value is not 123 when number = 1234");
    QVERIFY2(index == 4, "Index is not 4");

    number = NM_getLastDigitIndexUInt(number, &index);
    QVERIFY2(number == 12, "Returned value is not 12 when number = 123");
    QVERIFY2(index == 3, "Index is not 3");

    number = NM_getLastDigitIndexUInt(number, &index);
    QVERIFY2(number == 1, "Returned value is not 1 when number = 12");
    QVERIFY2(index == 2, "Index is not 2 when number = 12");

    number = NM_getLastDigitIndexUInt(number, &index);
    QVERIFY2(number == 0, "Returned value is not 0 when number = 1");
    QVERIFY2(index == 1, "Index is not 1 when number = 1");

    number = NM_getLastDigitIndexUInt(number, &index);
    QVERIFY2(number == 0, "Returned value is not 0 when number = 0");
    QVERIFY2(index == 0, "Index is not 0");
}

void NumberManipulationTests::lastDigitIndexULongShouldReturn0()
{
    unsigned long number = 0;
    Byte index;
    unsigned long newNumber = NM_getLastDigitIndexULong(number, &index);
    QVERIFY2(newNumber == 0, "Returned value is not 0 when number = 0");
    QVERIFY2(index == 0, "Index is not 0");
}

void NumberManipulationTests::lastDigitIndexULongWith1234567890ShouldReturnCorrectIndicesAndNumbers()
{
    unsigned long number = 1234567890;
    Byte index;

    number = NM_getLastDigitIndexULong(number, &index);
    QVERIFY2(number == 123456789, "Returned value is not 123456789 when number = 1234567890");
    QVERIFY2(index == 0, "Index is not 0");

    number = NM_getLastDigitIndexULong(number, &index);
    QVERIFY2(number == 12345678, "Returned value is not 12345678 when number = 123456789");
    QVERIFY2(index == 9, "Index is not 9");

    number = NM_getLastDigitIndexULong(number, &index);
    QVERIFY2(number == 1234567, "Returned value is not 1234567 when number = 12345678");
    QVERIFY2(index == 8, "Index is not 8");

    number = NM_getLastDigitIndexULong(number, &index);
    QVERIFY2(number == 123456, "Returned value is not 123456 when number = 1234567");
    QVERIFY2(index == 7, "Index is not 7");

    number = NM_getLastDigitIndexULong(number, &index);
    QVERIFY2(number == 12345, "Returned value is not 12345 when number = 123456");
    QVERIFY2(index == 6, "Index is not 6");

    number = NM_getLastDigitIndexULong(number, &index);
    QVERIFY2(number == 1234, "Returned value is not 1234 when number = 12345");
    QVERIFY2(index == 5, "Index is not 5");

    number = NM_getLastDigitIndexULong(number, &index);
    QVERIFY2(number == 123, "Returned value is not 123 when number = 1234");
    QVERIFY2(index == 4, "Index is not 4");

    number = NM_getLastDigitIndexULong(number, &index);
    QVERIFY2(number == 12, "Returned value is not 12 when number = 123");
    QVERIFY2(index == 3, "Index is not 3");

    number = NM_getLastDigitIndexULong(number, &index);
    QVERIFY2(number == 1, "Returned value is not 1 when number = 12");
    QVERIFY2(index == 2, "Index is not 2 when number = 12");

    number = NM_getLastDigitIndexULong(number, &index);
    QVERIFY2(number == 0, "Returned value is not 0 when number = 1");
    QVERIFY2(index == 1, "Index is not 1 when number = 1");

    number = NM_getLastDigitIndexULong(number, &index);
    QVERIFY2(number == 0, "Returned value is not 0 when number = 0");
    QVERIFY2(index == 0, "Index is not 0");
}

QTEST_APPLESS_MAIN(NumberManipulationTests)

#include "tst_number_manipulationtest.moc"
