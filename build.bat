@echo off

echo ----------------------------------------------------------------------------------------------------
echo Converting graphics
echo ----------------------------------------------------------------------------------------------------

echo ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
echo Presentation screen content
echo ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
del .\src\bank1*.* /Q
del .\bank1\*.* /Q

REM color_reducer\color_reducer.exe .\graphics\presentation_screen_1.png .\graphics\sms_palette.png 16 .\graphics\presentation_screen_1_sms.png
color_reducer\color_reducer.exe .\graphics\presentation_screen_2.png .\graphics\sms_palette.png 16 .\graphics\presentation_screen_2_sms.png
REM color_reducer\color_reducer.exe .\graphics\game_background.png .\graphics\sms_palette.png 16 .\graphics\game_background_sms.png
REM color_reducer\color_reducer.exe .\graphics\gems.png .\graphics\sms_palette.png 16 .\graphics\sprites_sms.png

REM .\bmp2tile\bmp2tile.exe .\graphics\presentation_screen_1_sms.png -mirror -removedupes -savetiles .\bank1\presentation_screen_1.bin -palsms -savepalette .\bank1\presentation_screen_1Palette.bin -savetilemap .\bank1\presentation_screen_1TileMap.bin -exit

.\bmp2tile\bmp2tile.exe .\graphics\gemitas_title_screen_3.png -mirror -removedupes -savetiles .\bank1\presentation_screen.bin -palsms -savepalette .\bank1\presentation_screenPalette.bin -savetilemap .\bank1\presentation_screenTileMap.bin -exit

folder2c .\bank1 .\src\bank1

echo ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
echo Game content
echo ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

del .\src\bank2*.* /Q
del .\bank2\*.* /Q

REM .\bmp2tile\bmp2tile.exe .\graphics\game_background_sms.png -mirror -removedupes -savetiles .\bank2\gameBackground.bin -palsms -savepalette .\bank2\gameBackgroundPalette.bin -savetilemap .\bank2\gameBackgroundTileMap.bin -exit
.\bmp2tile\bmp2tile.exe .\graphics\game_background_3.png -mirror -removedupes -savetiles .\bank2\gameBackground.bin -palsms -savepalette .\bank2\gameBackgroundPalette.bin -savetilemap .\bank2\gameBackgroundTileMap.bin -exit

.\bmp2tile\bmp2tile.exe .\graphics\sprites.png -nomirror -noremovedupes -savetiles .\bank2\spritestiles.bin -palsms -savepalette .\bank2\spritespalette.bin -exit
folder2c .\bank2 .\src\bank2

echo ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
echo Sound
echo ooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
call sound\build_sound.bat
folder2c .\bank3 .\src\bank3

call compile.bat

echo -
echo ----------------------------------------------------------------------------------------------------
echo Converting build to ROM
echo ----------------------------------------------------------------------------------------------------
ihx2sms gemitas.ihx gemitas.sms

Emulicious.jar gemitas.sms