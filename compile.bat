@echo off

del gemitas.sms
del gemitas.ihx
del gemitas.rel

echo ====================================================================================================
echo Compiling GEMITAS
echo ====================================================================================================


sdcc -c -mz80 --peep-file peep-rules.txt .\src\gemitas.c
sdcc -c -mz80 --peep-file peep-rules.txt .\src\number_manipulation.c
sdcc -c -mz80 --peep-file peep-rules.txt .\src\utils.c
sdcc -c -mz80 --peep-file peep-rules.txt .\src\array_utils.c
sdcc -c -mz80 --peep-file peep-rules.txt .\src\score_controller.c
sdcc -c -mz80 --peep-file peep-rules.txt .\src\board_tm.c
sdcc -c -mz80 --peep-file peep-rules.txt .\src\game_matrix.c
sdcc -c -mz80 --peep-file peep-rules.txt .\src\gems_block.c
sdcc -c -mz80 --peep-file peep-rules.txt .\src\state_machine.c
sdcc -c -mz80 --peep-file peep-rules.txt .\src\main_menu.c
sdcc -c -mz80 --peep-file peep-rules.txt .\src\presentation_screen.c
sdcc -c -mz80 --peep-file peep-rules.txt .\src\game.c
sdcc -c -mz80 --peep-file peep-rules.txt .\src\palette.c
sdcc -c -mz80 --peep-file peep-rules.txt .\src\gem_tiles.c

sdcc -c -mz80 --peep-file peep-rules.txt --constseg BANK2 .\src\bank1.c
sdcc -c -mz80 --peep-file peep-rules.txt --constseg BANK3 .\src\bank2.c
sdcc -c -mz80 --peep-file peep-rules.txt --constseg BANK4 .\src\bank3.c


sdcc -o gemitas.ihx -mz80 --no-std-crt0 --data-loc 0xC000 -Wl-b_BANK2=0x8000 -Wl-b_BANK3=0x8000 crt0_sms.rel gemitas.rel number_manipulation.rel utils.rel array_utils.rel score_controller.rel board_tm.rel game_matrix.rel gems_block.rel state_machine.rel main_menu.rel presentation_screen.rel game.rel palette.rel gem_tiles.rel SMSlib.lib PSGLib.rel bank1.rel bank2.rel bank3.rel
