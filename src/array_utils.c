#include "array_utils.h"
#include "types.h"


void AU_compactArray(Byte* array, Byte arraySize, Byte emptyValue)
{
    Byte writeIndex = 0;
    Byte readIndex = 0;

    while(array[readIndex] != emptyValue)
    {
        ++ readIndex;
    }

    if(readIndex >= arraySize - 1)
    {
        return;
    }

    writeIndex = readIndex;
    ++ readIndex;

    while(readIndex < arraySize)
    {
        if(array[readIndex] != emptyValue)
        {
            array[writeIndex] = array[readIndex];

            ++ writeIndex;
        }
        ++ readIndex;
    }

    for(Byte i = writeIndex; i < arraySize; ++ i)
    {
        array[i] = emptyValue;
    }
}

void AU_compactReverseArray(Byte* array, Byte arraySize, Byte emptyValue)
{
    int writeIndex = arraySize - 1;
    int readIndex = writeIndex;

    while(array[readIndex] != emptyValue)
    {
        -- readIndex;
    }

    if(readIndex == 0)
    {
        return;
    }

    writeIndex = readIndex;
    -- readIndex;

    while(readIndex >= 0)
    {
        if(array[readIndex] != emptyValue)
        {
            array[writeIndex] = array[readIndex];

            -- writeIndex;
        }
        -- readIndex;
    }

    for(Byte i = 0; i < writeIndex + 1; ++ i)
    {
        array[i] = emptyValue;
    }
}

void AU_rotateArrayToRight(Byte* array, Byte arraySize)
{
    const Byte swapValue = array[arraySize - 1];

    for(Byte i = arraySize - 1; i > 0; -- i)
    {
        array[i] = array[i - 1];
    }

    array[0] = swapValue;
}

void AU_rotateArrayToLeft(Byte* array, Byte arraySize)
{
    const Byte swapValue = array[0];

    for(Byte i = 0; i < arraySize - 1; ++ i)
    {
        array[i] = array[i + 1];
    }

    array[arraySize - 1] = swapValue;
}
