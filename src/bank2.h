extern const unsigned char	gameBackground_bin[];
#define				gameBackground_bin_size 3520

extern const unsigned char	gameBackgroundPalette_bin[];
#define				gameBackgroundPalette_bin_size 16

extern const unsigned char	gameBackgroundTileMap_bin[];
#define				gameBackgroundTileMap_bin_size 2048

extern const unsigned char	spritespalette_bin[];
#define				spritespalette_bin_size 14

extern const unsigned char	spritestiles_bin[];
#define				spritestiles_bin_size 1536

