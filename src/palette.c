#include "palette.h"
#include "SMSlib.h"
#include "utils.h"
#include "string.h"

#define PALETTE_SIZE 16
#define OFFSET 1
#define DELAY_FRAMES 5

static Byte currentBackgroundPalette[PALETTE_SIZE];
static Byte currentSpritesPalette[PALETTE_SIZE];
static Byte delayFrames = 0;

void Palette_initFadeOut(void* backgroundPalette, void* spritesPalette)
{
    if(backgroundPalette != NULL)
    {
        memcpy(currentBackgroundPalette, backgroundPalette, PALETTE_SIZE);
    }
    else
    {
        memset(currentBackgroundPalette, 0, sizeof(currentBackgroundPalette));
    }

    if(spritesPalette != NULL)
    {
        memcpy(currentSpritesPalette, spritesPalette, PALETTE_SIZE);
    }
    else
    {
        memset(currentSpritesPalette, 0, sizeof(currentSpritesPalette));
    }
    delayFrames = 0;
}

bool Palette_updateFadeOut()
{
    Byte blackNumber = 0;

    ++ delayFrames;

    if(delayFrames < DELAY_FRAMES)
    {
        return false;
    }

    delayFrames = 0;

    for(Byte i = 0; i < PALETTE_SIZE; ++ i)
    {
        {
            const Byte color = currentBackgroundPalette[i];
            const Byte r = max(red(color) - OFFSET, 0);
            const Byte g = max(green(color) - OFFSET, 0);
            const Byte b = max(blue(color) - OFFSET, 0);

            if(r == 0 && g == 0 && b == 0)
            {
                ++ blackNumber;
            }

            currentBackgroundPalette[i] = RGB(r, g, b);
        }
        {
            const Byte color = currentSpritesPalette[i];
            const Byte r = max(red(color) - OFFSET, 0);
            const Byte g = max(green(color) - OFFSET, 0);
            const Byte b = max(blue(color) - OFFSET, 0);

            if(r == 0 && g == 0 && b == 0)
            {
                ++ blackNumber;
            }

            currentSpritesPalette[i] = RGB(r, g, b);
        }
    }

    SMS_loadBGPalette(currentBackgroundPalette);
    SMS_loadSpritePalette(currentSpritesPalette);

    return blackNumber == PALETTE_SIZE * 2;
}


