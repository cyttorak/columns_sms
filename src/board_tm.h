#ifndef _TILE_MAP_H_
#define _TILE_MAP_H_

#include "types.h"
#include "constants.h"

#define BoardTM_DESTRUCTION_ANIMATION_FRAMES 7

#define BoardTM_LEFT_TILES 1
#define BoardTM_TOP_TILES 1
#define BoardTM_WIDTH_TILES 6
#define BoardTM_HEIGHT_TILES 13

#define BoardTM_X_PIXELS (BoardTM_LEFT_TILES * TILE_WIDTH)
#define BoardTM_Y_PIXELS (BoardTM_TOP_TILES * TILE_HEIGHT)
#define BoardTM_WIDTH_PIXELS (BoardTM_WIDTH_TILES * TILE_WIDTH * 2)
#define BoardTM_HEIGHT_PIXELS (BoardTM_HEIGHT_TILES * TILE_HEIGHT * 2)

#define BoardTM_LEFT_PIXELS BoardTM_X_PIXELS
#define BoardTM_RIGHT_PIXELS (BoardTM_X_PIXELS + BoardTM_WIDTH_PIXELS)
#define BoardTM_TOP_PIXELS BoardTM_Y_PIXELS
#define BoardTM_BOTTOM_PIXELS (BoardTM_Y_PIXELS + BoardTM_HEIGHT_PIXELS)

// "Empty" means black background with gray lines, and those tiles are behind sprites, not on top
void BoardTM_makeEmpty();
void BoardTM_setTileEmpty(signed char x, signed char y);
void BoardTM_setTileGem(signed char x, signed char y, Byte index);

void BoardTM_setupGemDestruction();
bool BoardTM_update();
void BoardTM_refreshFromGameMatrix();

void BoardTM_resetDestructionAnimation();
void BoardTM_setDestructionAnimation(signed char x, signed char y);
bool BoardTM_updateDestructionAnimation();

#endif
