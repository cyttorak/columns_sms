#include "score_controller.h"
#include "utils.h"
#include "constants.h"
#include "game_matrix.h"
#include "gems_block.h"
#include <stdlib.h>
#include "game.h"
#include "PSGlib.h"
#include "bank3.h"

#define MAXIMUM_SCORE 0xFFFFFFFF // unsigned long is 32 bits
#define LEVEL_SCORE_CHUNK 50

extern unsigned int framesSinceBeginning;

static const signed char LevelDelaysAndOffsets[10] =
{
    -5,
    -4,
    -3,
    -2,
    -1,
    1,
    2,
    3,
    4,
    5
};

static unsigned long score;
static unsigned long loop;
static unsigned long level;
static unsigned long consumedLevelUpScore;
static int multiplier;
static Byte comboCounter;

static unsigned char* destroySfxs[] =
{
    sfx_block_destroy_1_psg,
    sfx_block_destroy_2_psg,
    sfx_block_destroy_3_psg,
    sfx_block_destroy_4_psg,
    sfx_block_destroy_5_psg,
    sfx_block_destroy_6_psg,
    sfx_block_destroy_7_psg,
    sfx_block_destroy_8_psg,
};

#define updateScore() U_drawNumberULong(score, 14, 10, NUMBER_TILES_BASE_INDEX)
#define updateLoop() U_drawNumberULong(loop, 14, 13, NUMBER_TILES_BASE_INDEX)
#define updateLevel() U_drawNumberULong(level, 14, 16, NUMBER_TILES_BASE_INDEX)


void ScoreController_init()
{
    score = 0;
    loop = 0;
    level = 0;
    consumedLevelUpScore = 0;

    U_drawText("SCORE", 14, 9, LETTER_TILES_BASE_INDEX);
    updateScore();

    U_drawText("LOOP", 14, 12, LETTER_TILES_BASE_INDEX);
    updateLoop();

    U_drawText("LEVEL", 14, 15, LETTER_TILES_BASE_INDEX);
    updateLevel();

    GemsBlock_setStepDelay(LevelDelaysAndOffsets[0]);

    srand(framesSinceBeginning);
}

void ScoreController_startNewRound()
{
    multiplier = 1;
    comboCounter = 0;
}

void ScoreController_update()
{
    const unsigned long pointsToAdd = GameMatrix_destroyedGemNumber * multiplier;
    const unsigned long pointsToOverflow = MAXIMUM_SCORE - score;

    PSGSFXPlay(destroySfxs[comboCounter], SFX_CHANNEL2);
    comboCounter = min(comboCounter + 1, sizeof(destroySfxs) - 1);

    multiplier <<= 1;

    if(pointsToAdd > pointsToOverflow)
    {
        score = pointsToAdd - pointsToOverflow;

        ++ loop;
        updateLoop();

        consumedLevelUpScore = 0;
    }
    else
    {
        const unsigned long oldLevel = level;

        score += pointsToAdd;
        level = score / LEVEL_SCORE_CHUNK;

        if(oldLevel < level)
        {
            const Byte levelDelayIndex =  level % sizeof(LevelDelaysAndOffsets);

            updateLevel();

            GemsBlock_setStepDelay(LevelDelaysAndOffsets[levelDelayIndex]);

            srand(framesSinceBeginning);

            Game_setNextBlockAsSpecial();
        }
    }

    updateScore();
}
