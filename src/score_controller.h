#ifndef SCORE_CONTROLLER_H
#define SCORE_CONTROLLER_H

#include "types.h"

void ScoreController_init();
void ScoreController_startNewRound();
void ScoreController_update();

#endif // SCORE_CONTROLLER_H
