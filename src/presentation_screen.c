#include "SMSlib.h"
#include "types.h"
#include "constants.h"
#include "bank2.h"
#include "palette.h"
#include "state_machine.h"
#include "utils.h"

#define INPUT_DELAY_FRAMES 50 * 2

static bool fadingOut = false;
extern struct SM_StateMachine mainStateMachine;
extern unsigned int framesSinceBeginning;


void enterPresentationState()
{
    SMS_mapROMBank(GAME_SCREEN_GRAPHICS_BANK);

    SMS_loadTiles(gameBackground_bin, BG_TILES_POSITION, gameBackground_bin_size);
    SMS_loadBGPalette(gameBackgroundPalette_bin);

    SMS_setBGScrollX(0);
    SMS_setBGScrollY(32);

    fadingOut = false;

    for(Byte i = 0; i < SCREEN_WIDTH_TILES; ++ i)
    {
        for(Byte j = 0; j < SCREEN_HEIGHT_TILES; ++ j)
        {
            SMS_setTileatXY(i, j, 19);
        }
    }

    U_drawText("GEMITAS", 12, 6, LETTER_TILES_BASE_INDEX);
    U_drawText("SMS POWER COMPETITIONS", 2, 9, LETTER_TILES_BASE_INDEX);
    U_drawNumberUInt(2018, 24, 9, NUMBER_TILES_BASE_INDEX);
    SMS_setTileatXY(24, 9, 19);

    U_drawText("GRAPHICS BY TETECHIN", 2, 13, LETTER_TILES_BASE_INDEX);
    U_drawText("AND FRANIKKU", 11, 14, LETTER_TILES_BASE_INDEX);
    U_drawText("MUSIC AND SOUNDS BY TIBONE", 2, 16, LETTER_TILES_BASE_INDEX);
    U_drawText("CODE BY CYTO", 2, 18, LETTER_TILES_BASE_INDEX);
}

void updatePresentationState()
{
    hideAllSprites();

    if(!fadingOut)
    {
        if(framesSinceBeginning >= 50 * 5)
        {
            Palette_initFadeOut(gameBackgroundPalette_bin, NULL);
            fadingOut = true;
        }
    }
    else
    {
        if(Palette_updateFadeOut())
        {
            SM_setState(&mainStateMachine, MainMenuState);
        }
    }
}
