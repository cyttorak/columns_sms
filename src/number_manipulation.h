#ifndef NUMBERMANIPULATION_H
#define NUMBERMANIPULATION_H

#include "types.h"

char NM_getLastDigitIndexChar(signed char number, Byte* index);
unsigned int NM_getLastDigitIndexUInt(unsigned int number, Byte* index);
unsigned int NM_getLastDigitIndexULong(unsigned long number, Byte* index);


#endif // NUMBERMANIPULATION_H
