#ifndef PALETTE_H
#define PALETTE_H

#include "types.h"

void Palette_initFadeOut(void* backgroundPalette, void* spritesPalette);
bool Palette_updateFadeOut();

// Color is defined as least significant 6 bit in --BBGGRR format
#define red(color) ((color) & 0x03)
#define green(color) (((color) & 0x0C) >> 2)
#define blue(color) (((color) & 0x30) >> 4)

#endif // PALETTE_H
