#include "game.h"
#include "SMSlib.h"
#include "PSGlib.h"
#include "bank2.h"
#include "bank3.h"
#include "constants.h"
#include "board_tm.h"
#include "game_matrix.h"
#include "string.h"
#include "types.h"
#include "state_machine.h"
#include "gems_block.h"
#include <stdlib.h>
#include <math.h>
#include "utils.h"
#include "palette.h"
#include "gem_tiles.h"
#include "score_controller.h"

#define OVER_SPRITES_TILE_NUMBER 12

enum GameStates
{
    Intro,
    BlockMoving,
    BoardUpdating,
    GameOver,
    GameStateNumber
};

extern struct SM_StateMachine mainStateMachine;

struct SM_StateMachine gameStateMachine;
static struct SM_State gameStates[GameStateNumber];
static Byte frameCounter = 0;
static bool fadingOut = false;
static GemsBlock currentBlock;
static GemsBlock nextBlock;
static bool nextBlockIsSpecial;


static void setupBackground()
{
    SMS_setBGScrollX(0);
    SMS_setBGScrollY(32);

    // We asume destruction animation is always at the end of background tiles
    SMS_loadTiles(gameBackground_bin, BG_TILES_POSITION, gameBackground_bin_size - (BoardTM_DESTRUCTION_ANIMATION_FRAMES - 1) * BYTES_PER_TILE);
    //SMS_loadTiles(gameBackground_bin, BG_TILES_POSITION, gameBackground_bin_size);
    SMS_loadTiles(spritestiles_bin, SPRITE_TILES_POSITION, (GEM_NUMBER * 2) * 4 * BYTES_PER_TILE);

    {
        Byte tileMapCopy[gameBackgroundTileMap_bin_size];
        unsigned int* pTileMap = tileMapCopy + sizeof(unsigned int); // Ignore first tile

        memcpy(tileMapCopy, gameBackgroundTileMap_bin, gameBackgroundTileMap_bin_size);

        for(size_t i = 0; i < OVER_SPRITES_TILE_NUMBER; ++ i, ++ pTileMap)
        {
            (*pTileMap) |= TILE_PRIORITY;
        }

        SMS_loadTileMap(0, 0, tileMapCopy, gameBackgroundTileMap_bin_size);
    }

    SMS_loadBGPalette(gameBackgroundPalette_bin);

    ScoreController_init();
}

#define NEXT_GEMS_BLOCK_X 14
#define NEXT_GEMS_BLOCK_Y 2

static void updateNextGemsBlock()
{
    if(nextBlock[0] == SPECIAL_GEM_INDEX)
    {
        for(Byte i = 0; i < sizeof(nextBlock); ++ i)
        {
            SMS_loadTileMapArea(NEXT_GEMS_BLOCK_X, NEXT_GEMS_BLOCK_Y + i * 2, SpecialGemTiles, 2, 2);
        }
    }
    else
    {
        for(Byte i = 0; i < sizeof(nextBlock); ++ i)
        {
            SMS_loadTileMapArea(NEXT_GEMS_BLOCK_X, NEXT_GEMS_BLOCK_Y + i * 2, GemTiles[nextBlock[sizeof(nextBlock) - 1 - i]], 2, 2);
        }
    }
}

static void setupGemBlocks()
{
    // For some reason we cannot assign the random values to the arrays so we use some intermediary variables
    const Byte currentGem1 = rand() % (GEM_NUMBER - 1);
    const Byte currentGem2 = rand() % (GEM_NUMBER - 1);
    const Byte currentGem3 = rand() % (GEM_NUMBER - 1);

    const Byte nextGem1 = rand() % (GEM_NUMBER - 1);
    const Byte nextGem2 = rand() % (GEM_NUMBER - 1);
    const Byte nextGem3 = rand() % (GEM_NUMBER - 1);

    currentBlock[0] = currentGem1;
    currentBlock[1] = currentGem2;
    currentBlock[2] = currentGem3;

    nextBlock[0] = nextGem1;
    nextBlock[1] = nextGem2;
    nextBlock[2] = nextGem3;
}

static void calculateGemBlocks()
{
    memcpy(currentBlock, nextBlock, sizeof(currentBlock));

    if(nextBlockIsSpecial)
    {
        nextBlockIsSpecial = false;

        nextBlock[0] = SPECIAL_GEM_INDEX;
        nextBlock[1] = SPECIAL_GEM_INDEX;
        nextBlock[2] = SPECIAL_GEM_INDEX;
    }
    else
    {
        const Byte nextGem1 = rand() % (GEM_NUMBER - 1);
        const Byte nextGem2 = rand() % (GEM_NUMBER - 1);
        const Byte nextGem3 = rand() % (GEM_NUMBER - 1);

        nextBlock[0] = nextGem1;
        nextBlock[1] = nextGem2;
        nextBlock[2] = nextGem3;
    }

    updateNextGemsBlock();
}

static void enterIntroState()
{
    SMS_mapROMBank(GAME_SCREEN_GRAPHICS_BANK);
    GameMatrix_clear();
    BoardTM_makeEmpty();
    frameCounter = 0;
    setupGemBlocks();
    hideAllSprites();
}

static void updateIntroState()
{
    hideAllSprites();

    if(frameCounter < 2)
    {
        ++ frameCounter;
    }
    if(frameCounter == 1)
    {
        setupBackground();
        SMS_loadSpritePalette(spritespalette_bin);
        PSGPlayNoRepeat(intro_psg);
        return;
    }

    if(PSGGetStatus() == PSG_STOPPED)
    {
        PSGPlay(rand() % 2 == 0? bgm1_psg : bgm2_psg);
        SM_setState(&gameStateMachine, BlockMoving);
    }
}

static void enterBlockMovingState()
{
    hideAllSprites();

    calculateGemBlocks();
    GemsBlock_setGemsBlock(currentBlock);
    GemsBlock_setPosition(BoardTM_LEFT_PIXELS + GemsBlock_StartX * TILE_WIDTH * 2, BoardTM_TOP_PIXELS);
}

static void updateBlockMovingState()
{
    Byte drawnSprites = 0;
    bool exitState = false;

    if(!GemsBlock_update())
    {
        exitState = true;
    }

    drawnSprites = GemsBlock_draw();

//    for(Byte i = 0; i < 3; ++ i)
//    {
//        U_drawNumberChar(currentBlock[i], 0, i, NUMBER_TILES_BASE_INDEX);
//    }

    for(Byte i = drawnSprites; i < SPRITE_NUMBER; ++ i)
    {
        SMS_hideSprite(SMS_addSprite(0, 0, 0));
    }

    // game matrix <--> board continous tile setting for testing
//    for(Byte i = 0; i < GameMatrix_HEIGHT; ++ i)
//    {
//        for(Byte j = 0; j < GameMatrix_WIDTH; ++ j)
//        {
//            if(GameMatrix_gameMatrix[j][i] == GameMatrix_NoGemValue)
//            {
//                BoardTM_setTileEmpty(j, i);
//            }
//            else
//            {
//                BoardTM_setTileGem(j, i, GameMatrix_gameMatrix[j][i]);
//            }
//        }
//    }

    if(exitState)
    {
        ScoreController_startNewRound();
        SM_setState(&gameStateMachine, BoardUpdating);
    }
}

static void enterBoardUpdatingState()
{
    GameMatrix_calculateDestroyedGems();

    if(GameMatrix_destroyedGemNumber > 0)
    {
        BoardTM_setupGemDestruction();
        ScoreController_update();
    }
}

static void updateBoardUpdatingState()
{
    hideAllSprites();

    if(!BoardTM_update())
    {
        GameMatrix_removeDestroyedGems();
        BoardTM_refreshFromGameMatrix();
        SM_setState(&gameStateMachine, BoardUpdating);
    }
    else if(GameMatrix_destroyedGemNumber == 0)
    {
        SM_setState(&gameStateMachine, GameMatrix_isGameOver()? GameOver : BlockMoving);
    }
}

static void enterGameOverState()
{
    PSGPlayNoRepeat(game_over_psg);

    frameCounter = 0;
    fadingOut = false;

    for(Byte j = 0; j < 3; ++ j)
    {
        for(Byte i = 0; i < BoardTM_WIDTH_TILES * 2; ++ i)
        {
            SMS_setTileatXY(BoardTM_LEFT_TILES + i, 10 + j, 19);
        }
    }

    U_drawText("GAME", BoardTM_LEFT_TILES + BoardTM_WIDTH_TILES - 5, 11, LETTER_TILES_BASE_INDEX);
    U_drawText("OVER", BoardTM_LEFT_TILES + BoardTM_WIDTH_TILES, 11, LETTER_TILES_BASE_INDEX);
}

static void updateGameOverState()
{
    hideAllSprites();

    if(!fadingOut && PSGGetStatus() == PSG_STOPPED)
    {
        fadingOut = true;
        Palette_initFadeOut(gameBackgroundPalette_bin, spritespalette_bin);
    }


    if(fadingOut)
    {
        if(Palette_updateFadeOut())
        {
            SM_setState(&mainStateMachine, MainMenuState);
        }
    }
}

static void initStateMachine()
{
    SM_initState(&gameStates[Intro], enterIntroState, updateIntroState, NULL);
    SM_initState(&gameStates[BlockMoving], enterBlockMovingState, updateBlockMovingState, NULL);
    SM_initState(&gameStates[BoardUpdating], enterBoardUpdatingState, updateBoardUpdatingState, NULL);
    SM_initState(&gameStates[GameOver], enterGameOverState, updateGameOverState, NULL);

    SM_init(&gameStateMachine, gameStates, GameStateNumber);
}

void enterGameState()
{
    nextBlockIsSpecial = false;

    initStateMachine();

    SM_setState(&gameStateMachine, Intro);
}

void updateGameState()
{
    SM_update(&gameStateMachine);
}

void Game_setNextBlockAsSpecial()
{
    nextBlockIsSpecial = true;
}
