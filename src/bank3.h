extern const unsigned char	bgm1_psg[];
#define				bgm1_psg_size 4489

extern const unsigned char	bgm2_psg[];
#define				bgm2_psg_size 2459

extern const unsigned char	game_over_psg[];
#define				game_over_psg_size 145

extern const unsigned char	intro_psg[];
#define				intro_psg_size 595

extern const unsigned char	sfx_block_destroy_1_psg[];
#define				sfx_block_destroy_1_psg_size 35

extern const unsigned char	sfx_block_destroy_2_psg[];
#define				sfx_block_destroy_2_psg_size 35

extern const unsigned char	sfx_block_destroy_3_psg[];
#define				sfx_block_destroy_3_psg_size 35

extern const unsigned char	sfx_block_destroy_4_psg[];
#define				sfx_block_destroy_4_psg_size 34

extern const unsigned char	sfx_block_destroy_5_psg[];
#define				sfx_block_destroy_5_psg_size 35

extern const unsigned char	sfx_block_destroy_6_psg[];
#define				sfx_block_destroy_6_psg_size 34

extern const unsigned char	sfx_block_destroy_7_psg[];
#define				sfx_block_destroy_7_psg_size 34

extern const unsigned char	sfx_block_destroy_8_psg[];
#define				sfx_block_destroy_8_psg_size 34

extern const unsigned char	sfx_rotate_block_psg[];
#define				sfx_rotate_block_psg_size 19

extern const unsigned char	sfx_select_psg[];
#define				sfx_select_psg_size 6

extern const unsigned char	sfx_touch_psg[];
#define				sfx_touch_psg_size 7

extern const unsigned char	title_screen_psg[];
#define				title_screen_psg_size 1233

