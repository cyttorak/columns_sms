#include "SMSlib.h"
#include "PSGlib.h"
#include "types.h"
#include "constants.h"
#include "state_machine.h"
#include "presentation_screen.h"
#include "main_menu.h"
#include "game.h"


unsigned int framesSinceBeginning;

void initConsole()
{
    SMS_init();
    SMS_VDPturnOnFeature(VDPFEATURE_EXTRAHEIGHT);
    SMS_VDPturnOnFeature(VDPFEATURE_224LINES);
    SMS_setSpriteMode(SPRITEMODE_TALL);

    PSGSetMusicVolumeAttenuation(0);
}

struct SM_StateMachine mainStateMachine;
static struct SM_State gameStates[MainStateNumber];

void initStateMachine()
{
    SM_initState(&gameStates[PresentationState], enterPresentationState, updatePresentationState, NULL);
    SM_initState(&gameStates[MainMenuState], enterMainMenuState, updateMainMenuState, NULL);
    SM_initState(&gameStates[GameState], enterGameState, updateGameState, NULL);

    SM_init(&mainStateMachine, gameStates, MainStateNumber);
}


void main()
{
    framesSinceBeginning = 0;

	initConsole();

    initStateMachine();
    SM_setState(&mainStateMachine, PresentationState);

    SMS_displayOn();

	for(;;)
	{
        SMS_initSprites();
        SM_update(&mainStateMachine);
		SMS_waitForVBlank();
        PSGFrame();
        PSGSFXFrame();
        SMS_copySpritestoSAT();

        ++ framesSinceBeginning;
	}
}


SMS_EMBED_SEGA_ROM_HEADER(0, 1);
SMS_EMBED_SDSC_HEADER_AUTO_DATE(1, 0, "Cyto", "Gemitas", "Gemitas for SMS");
