#include "utils.h"
#include "SMSlib.h"
#include "number_manipulation.h"
#include "string.h"
#include "constants.h"


void hideAllSprites()
{
    for(Byte i = 0; i < SPRITE_NUMBER; ++ i)
    {
        SMS_hideSprite(SMS_addSprite(0, 0, 0));
    }
}

void U_drawNumberChar(Byte number, signed char tileX, signed char tileY, int tilesBaseIndex)
{
    signed char n = number;
    signed char currentX = tileX + 2;

    for(signed char i = 0; i < 3; ++ i)
	{
        Byte index;
        n = NM_getLastDigitIndexChar(n, &index);
        SMS_setTileatXY(currentX, tileY, tilesBaseIndex + index);
		-- currentX;
	}
}

void U_drawNumberUInt(unsigned int number, signed char tileX, signed char tileY, int tilesBaseIndex)
{
	unsigned int n = number;
    signed char currentX = tileX + 4;

    for(signed char i = 0; i < 5; ++ i)
	{
        Byte index;
        n = NM_getLastDigitIndexUInt(n, &index);
        SMS_setTileatXY(currentX, tileY, tilesBaseIndex + index);
		-- currentX;
	}
}
void U_drawNumberULong(unsigned long number, signed char tileX, signed char tileY, int tilesBaseIndex)
{
    unsigned long n = number;
    signed char currentX = tileX + 9;
    Byte index;

    for(signed char i = 0; i < 10; ++ i)
    {
        // Same issue as with rand(): it seems Z80 or whatever does not like to return values from functions from time to time, so we inline function's body right here
        //const unsigned long currentNumber = NM_getLastDigitIndexULong(n, &index);
        if(n == 0)
        {
            index = 0;
        }
        else
        {
            index = n % 10;
            n /= 10;
        }

        SMS_setTileatXY(currentX, tileY, tilesBaseIndex + index);
        -- currentX;
    }
}

void U_drawText(const char* text, signed char tileX, signed char tileY, int tilesBaseIndex)
{
    char* currentChar = (char*)text;
    signed char currentX = tileX;

    while(*currentChar != 0)
    {
        if(*currentChar != ' ')
        {
            SMS_setTileatXY(currentX, tileY, tilesBaseIndex + *currentChar - 'A');
        }
        ++ currentChar;
        ++ currentX;
    }
}
