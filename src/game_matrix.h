#ifndef _GAME_MATRIX_H_
#define _GAME_MATRIX_H_

#include "types.h"
#include "gems_block.h"
#include "constants.h"

#define GameMatrix_WIDTH 6
#define GameMatrix_HEIGHT 13
#define GameMatrix_NoGemValue GEM_NUMBER + 1

extern Byte GameMatrix_gameMatrix[GameMatrix_WIDTH][GameMatrix_HEIGHT];
extern struct Vector2D GameMatrix_destroyedGems[GameMatrix_WIDTH * GameMatrix_HEIGHT];
extern Byte GameMatrix_destroyedGemNumber;


#define GameMatrix_NoFreeY 0xFF

#define GameMatrix_clear() memset(GameMatrix_gameMatrix, GameMatrix_NoGemValue, sizeof(GameMatrix_gameMatrix));
#define GameMatrix_isFree(x, y) (GameMatrix_gameMatrix[(x)][(y)] == GameMatrix_NoGemValue)
#define GameMatrix_setGem(x, y, gem) GameMatrix_gameMatrix[(x)][(y)] = (gem);

#define GameMatrix_setColumn(columns, x) (columns) |= (1 << (x))
#define GameMatrix_isColumnSet(columns, x) (((columns) & (1 << (x))) != 0)
#define GameMatrix_setRow(rows, y) (rows) |= (1 << (y))
#define GameMatrix_isRowSet(rows, y) (((rows) & (1 << (y))) != 0)

Byte GameMatrix_getTopFreeY(signed char x);

void GameMatrix_setSpecialGemPosition(signed char x, signed char y);

void GameMatrix_calculateDestroyedGems();
void GameMatrix_removeDestroyedGems();

bool GameMatrix_isGameOver();


#endif
