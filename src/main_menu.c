#include "SMSlib.h"
#include "PSGlib.h"
#include "types.h"
#include "constants.h"
#include "bank1.h"
#include "bank3.h"
#include "palette.h"
#include "state_machine.h"
#include "utils.h"

#define INPUT_DELAY_FRAMES 50 * 2

static unsigned int frameCounter = 0;
static bool fadingOut = false;
extern struct SM_StateMachine mainStateMachine;


void enterMainMenuState()
{
    SMS_mapROMBank(MAIN_MENU_GRAPHICS_BANK);

    SMS_loadTiles(presentation_screen_bin, BG_TILES_POSITION, presentation_screen_bin_size);
    SMS_loadTileMap(0, 0, presentation_screenTileMap_bin, presentation_screenTileMap_bin_size);

    SMS_setBGScrollX(0);
    SMS_setBGScrollY(32);

    frameCounter = 0;
    fadingOut = false;
}

void updateMainMenuState()
{
    hideAllSprites();

    ++ frameCounter;
    // Let one frame of blank screen before loading the palette
    if(frameCounter == 1)
    {
        SMS_loadBGPalette(presentation_screenPalette_bin);

        SMS_mapROMBank(GAME_SCREEN_GRAPHICS_BANK);
        PSGPlay(title_screen_psg);

        return;
    }
    if(frameCounter < INPUT_DELAY_FRAMES)
    {
        return;
    }

    frameCounter = INPUT_DELAY_FRAMES;

    if(!fadingOut)
    {
        const unsigned int keys = SMS_getKeysReleased();

        if(keys == PORT_A_KEY_1 || keys == PORT_A_KEY_2)
        {
            PSGStop(); // Stop music because palette is in another ROM bank and music works with streaming
            SMS_mapROMBank(MAIN_MENU_GRAPHICS_BANK);
            Palette_initFadeOut(presentation_screenPalette_bin, NULL);
            fadingOut = true;
        }
    }
    else if(Palette_updateFadeOut())
    {
        SM_setState(&mainStateMachine, GameState);
    }
}
