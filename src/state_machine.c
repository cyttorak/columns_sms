#include "state_machine.h"

void SM_initState(struct SM_State* state, SM_Call* enter, SM_Call* update, SM_Call* leave)
{
    state->enter = enter;
    state->update = update;
    state->leave = leave;
}

void SM_init(struct SM_StateMachine* stateMachine, struct SM_State* states, Byte stateNumber)
{
    stateMachine->states = states;
    stateMachine->stateNumber = stateNumber;
    stateMachine->currentState = SM_INVALID_STATE;
}

void SM_setState(struct SM_StateMachine* stateMachine, Byte stateId)
{
    SM_Call* leaveCall = stateMachine->states[stateMachine->currentState].leave;
    SM_Call* enterCall = stateMachine->states[stateId].enter;

    if(leaveCall)
    {
        leaveCall();
    }

    stateMachine->currentState = stateId;

    if(enterCall)
    {
        enterCall();
    }
}

void SM_update(struct SM_StateMachine* stateMachine)
{
    stateMachine->states[stateMachine->currentState].update();
}
