#include "gems_block.h"
#include "string.h"
#include "SMSlib.h"
#include "PSGlib.h"
#include "constants.h"
#include "game_matrix.h"
#include "board_tm.h"
#include "utils.h"
#include "array_utils.h"
#include "bank3.h"


static GemsBlock currentGemsBlock;
static Byte tileBaseIndices[GemsBlock_BlockSize];
static struct Vector2D position;
static signed char delay;
static signed char currentDelay;
static Byte autoVerticalSpeed;

#define SPECIAL_GEM_FRAME_NUMBER 6
static Byte specialGemBlockFrame;

signed char blockBoardX;
signed char blockBoardY;
bool blockDidTouchGroundOrGem;
static Byte leftKeysFrameCounter = 0;
static Byte rightKeysFrameCounter = 0;
static Byte blockLockFrameCounter = 0;

static Byte i;


/*

  Block position is the position of lowest gem, so it is like this:

  [2]
  [1]
  [0]

  All calculations are done using [0] gem. In other words, gem blocks are defined bottom to top

*/

void GemsBlock_setStepDelay(signed char delayOrOffset)
{
    delay = delayOrOffset;
    currentDelay = -delay;
}

void GemsBlock_setGemsBlock(const GemsBlock gemsBlock)
{
    for(i = 0; i < sizeof(GemsBlock); ++ i)
    {
        currentGemsBlock[i] = gemsBlock[i];
        // Multiply by 4 because each gem takes 4 tiles
        tileBaseIndices[i] = currentGemsBlock[i] << 2;
    }

    if(currentGemsBlock[0] == SPECIAL_GEM_INDEX)
    {
        specialGemBlockFrame = 0;
    }
}

void GemsBlock_setPosition(signed char x, signed char y)
{
    position.x = x;
    position.y = y;
}

#define HORIZONTAL_SPEED TILE_WIDTH * 2
#define PLAYER_VERTICAL_SPEED 5
#define HORIZONTAL_INPUT_FRAME_NUMBER 5
#define LOCKING_FRAME_NUMBER 30

bool GemsBlock_update()
{
    if(SMS_getKeysHeld() == PORT_A_KEY_DOWN)
    {
        position.y += PLAYER_VERTICAL_SPEED;
        blockLockFrameCounter = LOCKING_FRAME_NUMBER;
    }

    if(SMS_getKeysHeld() == PORT_A_KEY_LEFT)
    {
        ++ leftKeysFrameCounter;
        rightKeysFrameCounter = 0;
    }
    else if(SMS_getKeysHeld() == PORT_A_KEY_RIGHT)
    {
        ++ rightKeysFrameCounter;
        leftKeysFrameCounter = 0;
    }
    else
    {
        leftKeysFrameCounter = 0;
        rightKeysFrameCounter = 0;
    }

    if(delay < 0)
    {
        -- currentDelay;
        if(currentDelay == 0)
        {
            currentDelay = -delay;
            ++ position.y;
        }
    }
    else
    {
        position.y += delay;
    }

    blockBoardX = (position.x - BoardTM_LEFT_PIXELS) / (TILE_WIDTH * 2);
    blockBoardY = (position.y - BoardTM_TOP_PIXELS) / (TILE_HEIGHT * 2);
    blockBoardY = (blockBoardY >= GameMatrix_HEIGHT || blockBoardY < 0)? 0 : blockBoardY;


    // Check if we are reached board's bottom or there is a gem below
    if(position.y >= BoardTM_BOTTOM_PIXELS - TILE_HEIGHT * 2)
    {
        position.y = BoardTM_BOTTOM_PIXELS - TILE_HEIGHT * 2;
        blockDidTouchGroundOrGem = true;
    }
    else if(blockBoardY + 1 < GameMatrix_HEIGHT)
    {
        if(!GameMatrix_isFree(blockBoardX, blockBoardY + 1))
        {
            blockBoardY = GameMatrix_getTopFreeY(blockBoardX);
            blockBoardY = (blockBoardY >= GameMatrix_HEIGHT || blockBoardY < 0)? 0 : blockBoardY;
            position.y = BoardTM_TOP_PIXELS + (blockBoardY * TILE_HEIGHT * 2);
            blockDidTouchGroundOrGem = true;
        }
        else
        {
            blockDidTouchGroundOrGem = false;
        }
    }
    else
    {
        blockDidTouchGroundOrGem = false;
    }

    if(blockDidTouchGroundOrGem)
    {
        ++ blockLockFrameCounter;
    }
    else
    {
        blockLockFrameCounter = 0;
    }

    if(blockLockFrameCounter >= LOCKING_FRAME_NUMBER)
    {
        blockLockFrameCounter = 0;

        if(currentGemsBlock[0] == SPECIAL_GEM_INDEX)
        {
            GameMatrix_setSpecialGemPosition(blockBoardX, blockBoardY);
        }
        else
        {
            for(i = 0; i < min(blockBoardY + 1, sizeof(GemsBlock)); ++ i)
            {
                BoardTM_setTileGem(blockBoardX, blockBoardY - i, currentGemsBlock[i]);
                GameMatrix_setGem(blockBoardX, blockBoardY - i, currentGemsBlock[i]);
            }
        }

        PSGSFXPlay(sfx_touch_psg, SFX_CHANNEL3);

        return false;
    }

    switch(SMS_getKeysPressed())
    {
        case PORT_A_KEY_1:
            AU_rotateArrayToLeft(currentGemsBlock, sizeof(currentGemsBlock));
            AU_rotateArrayToLeft(tileBaseIndices, sizeof(tileBaseIndices));
            PSGSFXPlay(sfx_rotate_block_psg, SFX_CHANNEL2);
            break;
        case PORT_A_KEY_2:
            AU_rotateArrayToRight(currentGemsBlock, sizeof(currentGemsBlock));
            AU_rotateArrayToRight(tileBaseIndices, sizeof(tileBaseIndices));
            PSGSFXPlay(sfx_rotate_block_psg, SFX_CHANNEL2);
            break;
        case PORT_A_KEY_LEFT:
            if(blockBoardX > 0 &&
               ((blockBoardY < GameMatrix_HEIGHT - 1 && GameMatrix_isFree(blockBoardX - 1, blockBoardY + 1)) ||
                (blockBoardY == GameMatrix_HEIGHT - 1 && GameMatrix_isFree(blockBoardX - 1, blockBoardY)))
               )
            {
                position.x = position.x - HORIZONTAL_SPEED;
                -- blockBoardX;
                leftKeysFrameCounter = 0;
            }
            break;
        case PORT_A_KEY_RIGHT:
            if(blockBoardX < GameMatrix_WIDTH - 1 &&
               ((blockBoardY < GameMatrix_HEIGHT - 1 && GameMatrix_isFree(blockBoardX + 1, blockBoardY + 1)) ||
                (blockBoardY == GameMatrix_HEIGHT - 1 && GameMatrix_isFree(blockBoardX + 1, blockBoardY)))
               )
            {
                position.x = position.x + HORIZONTAL_SPEED;
                ++ blockBoardX;
                rightKeysFrameCounter = 0;
            }
            break;
        default:
            break;
    }

    if(leftKeysFrameCounter >= HORIZONTAL_INPUT_FRAME_NUMBER)
    {
        if(blockBoardX > 0 &&
           ((blockBoardY < GameMatrix_HEIGHT - 1 && GameMatrix_isFree(blockBoardX - 1, blockBoardY + 1)) ||
            (blockBoardY == GameMatrix_HEIGHT - 1 && GameMatrix_isFree(blockBoardX - 1, blockBoardY)))
           )
        {
            position.x = position.x - HORIZONTAL_SPEED;
            -- blockBoardX;
        }
        leftKeysFrameCounter = 0;
    }
    else if(rightKeysFrameCounter >= HORIZONTAL_INPUT_FRAME_NUMBER)
    {
        if(blockBoardX < GameMatrix_WIDTH - 1 &&
           ((blockBoardY < GameMatrix_HEIGHT - 1 && GameMatrix_isFree(blockBoardX + 1, blockBoardY + 1)) ||
            (blockBoardY == GameMatrix_HEIGHT - 1 && GameMatrix_isFree(blockBoardX + 1, blockBoardY)))
           )
        {
            position.x = position.x + HORIZONTAL_SPEED;
            ++ blockBoardX;
        }
        rightKeysFrameCounter = 0;
    }

    return true;
}

Byte GemsBlock_draw()
{
    if(currentGemsBlock[0] == SPECIAL_GEM_INDEX)
    {
        const Byte baseIndex = (SPECIAL_GEM_INDEX + specialGemBlockFrame) << 2;

        for(i = 0; i < GemsBlock_BlockSize; ++ i)
        {
            tileBaseIndices[i] = baseIndex;
        }
        specialGemBlockFrame = (++ specialGemBlockFrame) % SPECIAL_GEM_FRAME_NUMBER;
    }

    SMS_addSprite(position.x, position.y, tileBaseIndices[0]);
    SMS_addSprite(position.x + TILE_WIDTH, position.y, tileBaseIndices[0] + 2);

    if(position.y > TILE_HEIGHT / 2 && position.y < SCREEN_HEIGHT_PIXELS)
    {
        SMS_addSprite(position.x, position.y - (TILE_HEIGHT << 1), tileBaseIndices[1]);
        SMS_addSprite(position.x + TILE_WIDTH, position.y - (TILE_HEIGHT << 1), tileBaseIndices[1] + 2);

        SMS_addSprite(position.x, position.y - (TILE_HEIGHT << 2), tileBaseIndices[2]);
        SMS_addSprite(position.x + TILE_WIDTH, position.y - (TILE_HEIGHT << 2), tileBaseIndices[2] + 2);
        return 6;
    }

    return 2;
}
