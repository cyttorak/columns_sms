#ifndef _TYPES_AND_DEFINES_H_
#define _TYPES_AND_DEFINES_H_

#define FRAMES_PER_SECOND 50

#define TILE_WIDTH 8
#define TILE_HEIGHT 8

#define SCREEN_WIDTH_PIXELS 256
#define SCREEN_HEIGHT_PIXELS 224
#define SCREEN_WIDTH_TILES (SCREEN_WIDTH_PIXELS / TILE_WIDTH)
#define SCREEN_HEIGHT_TILES (SCREEN_HEIGHT_PIXELS / TILE_HEIGHT)

#define SPRITE_NUMBER 64
#define BYTES_PER_TILE 32
#define BG_TILES_POSITION 0
#define SPRITE_TILES_POSITION 256
#define NUMBER_TILES_BASE_INDEX 66
#define LETTER_TILES_BASE_INDEX (NUMBER_TILES_BASE_INDEX + 11)

#define GEM_NUMBER 7
#define SPECIAL_GEM_INDEX (GEM_NUMBER - 1)
#define GEM_TILE_NUMBER 4
#define GEM_TILES_POSITION SPRITE_TILES_POSITION + 0

#define MAIN_MENU_GRAPHICS_BANK 2
#define GAME_SCREEN_GRAPHICS_BANK 3
#define SOUND_BANK 4


enum MainStates
{
    PresentationState,
    MainMenuState,
    GameState,
    MainStateNumber
};


#endif
