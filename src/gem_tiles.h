#ifndef GEM_TILES_H
#define GEM_TILES_H

#include "constants.h"
#include "SMSlib.h"

extern const int GemTiles[GEM_NUMBER - 1][GEM_TILE_NUMBER];
extern const int SpecialGemTiles[GEM_TILE_NUMBER];

#endif // GEM_TILES_H
