#include "number_manipulation.h"

char NM_getLastDigitIndexChar(signed char number, Byte* index)
{
    if(number == 0)
    {
        *index = 0;
        return 0;
    }
    *index = number % 10;
    return number / 10;
}

unsigned int NM_getLastDigitIndexUInt(unsigned int number, Byte* index)
{
    if(number == 0)
    {
        *index = 0;
        return 0;
    }
    *index = number % 10;
    return number / 10;
}

unsigned int NM_getLastDigitIndexULong(unsigned long number, Byte* index)
{
    if(number == 0)
    {
        *index = 0;
        return 0;
    }
    *index = number % 10;
    return number / 10;
}
