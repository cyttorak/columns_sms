#ifndef TYPES_H
#define TYPES_H

#define NULL 0

#define bool char
#define true 1
#define false 0

typedef unsigned char Byte;

typedef struct Vector2D
{
    Byte x;
    Byte y;
};

#endif // TYPES_H
