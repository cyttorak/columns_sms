#include "board_tm.h"
#include "SMSlib.h"
#include "constants.h"
#include "bank2.h"
#include "game_matrix.h"
#include "gem_tiles.h"

#define DESTRUCTION_ANIMATION_BASE_INDEX 103
#define DESTRUCTION_ANIMATION_WAIT_FRAMES 3

static const int BlankTile[] = {7, 8, 18, 19};
static const int DestructionTile[] = {DESTRUCTION_ANIMATION_BASE_INDEX,
                                      DESTRUCTION_ANIMATION_BASE_INDEX | TILE_FLIPPED_X,
                                      DESTRUCTION_ANIMATION_BASE_INDEX | TILE_FLIPPED_Y,
                                      DESTRUCTION_ANIMATION_BASE_INDEX | TILE_FLIPPED_X | TILE_FLIPPED_Y};

extern signed char blockBoardX;
extern signed char blockBoardY;

static Byte blinkNumber;
static Byte blinkFrameCounter;
static bool gemDestructionAnimationStarted;

void BoardTM_makeEmpty()
{
    for(signed char i = BoardTM_TOP_TILES; i < BoardTM_TOP_TILES + BoardTM_HEIGHT_TILES * 2; i += 2)
	{
        for(signed char j = BoardTM_LEFT_TILES; j < BoardTM_LEFT_TILES + BoardTM_WIDTH_TILES * 2; j += 2)
		{
			SMS_loadTileMapArea(j, i, BlankTile, 2, 2);
		}
	}
}

void BoardTM_setTileEmpty(signed char x, signed char y)
{
    SMS_loadTileMapArea(BoardTM_LEFT_TILES + (x << 1), BoardTM_TOP_TILES + (y << 1), BlankTile, 2, 2);
}

void BoardTM_setTileGem(signed char x, signed char y, Byte gemIndex)
{
    SMS_loadTileMapArea(BoardTM_LEFT_TILES + (x << 1), BoardTM_TOP_TILES + (y << 1), GemTiles[gemIndex], 2, 2);
}

static void setDestroyedGemsAsEmpty()
{
    for(Byte i = 0; i < GameMatrix_destroyedGemNumber; ++ i)
    {
        const struct Vector2D* position = &GameMatrix_destroyedGems[i];
        SMS_loadTileMapArea(BoardTM_LEFT_TILES + (position->x << 1), BoardTM_TOP_TILES + (position->y << 1), BlankTile, 2, 2);
    }
}

static void restoreDestroyedGems()
{
    for(Byte i = 0; i < GameMatrix_destroyedGemNumber; ++ i)
    {
        const struct Vector2D* position = &GameMatrix_destroyedGems[i];
        const Byte gemIndex = GameMatrix_gameMatrix[position->x][position->y];
        SMS_loadTileMapArea(BoardTM_LEFT_TILES + (position->x << 1), BoardTM_TOP_TILES + (position->y << 1), GemTiles[gemIndex], 2, 2);
    }
}

void BoardTM_setupGemDestruction()
{
    blinkNumber = 0;
    blinkFrameCounter = 0;
    gemDestructionAnimationStarted = false;

    setDestroyedGemsAsEmpty();
}

#define TOTAL_BLINK_NUMBER 10
#define BLINK_FRAMES_TO_WAIT 2

bool BoardTM_update()
{
    if(blinkNumber < TOTAL_BLINK_NUMBER)
    {
        if(blinkFrameCounter >= BLINK_FRAMES_TO_WAIT)
        {
            if(blinkNumber % 2 == 0)
            {
                restoreDestroyedGems();
            }
            else
            {
                setDestroyedGemsAsEmpty();
            }

            blinkFrameCounter = 0;
            ++ blinkNumber;
        }
        else
        {
            ++ blinkFrameCounter;
        }
    }
    else
    {
        if(gemDestructionAnimationStarted)
        {
            if(!BoardTM_updateDestructionAnimation())
            {
                setDestroyedGemsAsEmpty();
                return false;
            }
        }
        else
        {
            BoardTM_resetDestructionAnimation();

            for(Byte i = 0; i < GameMatrix_destroyedGemNumber; ++ i)
            {
                const struct Vector2D* position = &GameMatrix_destroyedGems[i];
                SMS_loadTileMapArea(BoardTM_LEFT_TILES + (position->x << 1), BoardTM_TOP_TILES + (position->y << 1), DestructionTile, 2, 2);
            }

            gemDestructionAnimationStarted = true;
        }
    }

    return true;
}

static char destructionAnimationFrame = DESTRUCTION_ANIMATION_BASE_INDEX;
static char destructionAnimationWaitFrames = 5;

void BoardTM_resetDestructionAnimation()
{
    destructionAnimationFrame = DESTRUCTION_ANIMATION_BASE_INDEX;
	destructionAnimationWaitFrames = DESTRUCTION_ANIMATION_WAIT_FRAMES;
}

void BoardTM_setDestructionAnimation(signed char x, signed char y)
{
    SMS_loadTileMapArea(BoardTM_LEFT_TILES + (x << 1), BoardTM_TOP_TILES + (y << 1), DestructionTile, 2, 2);
}

bool BoardTM_updateDestructionAnimation()
{
	if(destructionAnimationWaitFrames > 0)
	{
		-- destructionAnimationWaitFrames;
        return true;
	}

	destructionAnimationWaitFrames = DESTRUCTION_ANIMATION_WAIT_FRAMES;

	++ destructionAnimationFrame;

    if(destructionAnimationFrame > DESTRUCTION_ANIMATION_BASE_INDEX + (BoardTM_DESTRUCTION_ANIMATION_FRAMES - 1))
	{
        //destructionAnimationFrame = DESTRUCTION_ANIMATION_BASE_INDEX;
        return false;
	}

    SMS_loadTiles(&gameBackground_bin[destructionAnimationFrame * BYTES_PER_TILE], BG_TILES_POSITION + DESTRUCTION_ANIMATION_BASE_INDEX, BYTES_PER_TILE);

	return true;
}


void BoardTM_refreshFromGameMatrix()
{
    for(Byte i = 0; i < GameMatrix_HEIGHT; ++ i)
    {
        for(Byte j = 0; j < GameMatrix_WIDTH; ++ j)
        {
            if(GameMatrix_gameMatrix[j][i] == GameMatrix_NoGemValue)
            {
                SMS_loadTileMapArea(BoardTM_LEFT_TILES + (j << 1), BoardTM_TOP_TILES + (i << 1), BlankTile, 2, 2);
            }
            else
            {
                SMS_loadTileMapArea(BoardTM_LEFT_TILES + (j << 1), BoardTM_TOP_TILES + (i << 1), GemTiles[GameMatrix_gameMatrix[j][i]], 2, 2);
            }
        }
    }
}
