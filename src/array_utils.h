#ifndef ARRAY_UTILS_H
#define ARRAY_UTILS_H

#include "types.h"

void AU_compactArray(Byte* array, Byte arraySize, Byte emptyValue);
void AU_compactReverseArray(Byte* array, Byte arraySize, Byte emptyValue);

void AU_rotateArrayToRight(Byte* array, Byte arraySize);
void AU_rotateArrayToLeft(Byte* array, Byte arraySize);

#endif // ARRAY_UTILS_H
