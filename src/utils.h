#ifndef _UTILS_H_
#define _UTILS_H_

#include "types.h"

#define max(v1, v2) ((v1) > (v2)? (v1) : (v2))
#define min(v1, v2) ((v1) < (v2)? (v1) : (v2))
#define clamp(minimum, value, maximum) max(min((maximum), (value)), (minimum))

#define setHigh4(receiver, value) (receiver |= value << 4)
#define getHigh4(bitSet) (bitSet >> 4)
#define setLow4(receiver, value) (receiver |= value & 0x0F)
#define getLow4(bitSet) (bitSet & 0xF)

#define U_absolute(value) (value < 0)? -value : value

void hideAllSprites();

void U_drawNumberChar(Byte number, signed char tileX, signed char tileY, int tilesBaseIndex);
void U_drawNumberUInt(unsigned int number, signed char tileX, signed char tileY, int tilesBaseIndex);
void U_drawNumberULong(unsigned long number, signed char tileX, signed char tileY, int tilesBaseIndex);

void U_drawText(const char* text, signed char tileX, signed char tileY, int tilesBaseIndex);

#endif
