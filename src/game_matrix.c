#include "game_matrix.h"
#include <string.h>
#include "array_utils.h"
#include <stdlib.h>


Byte GameMatrix_gameMatrix[GameMatrix_WIDTH][GameMatrix_HEIGHT];
struct Vector2D GameMatrix_destroyedGems[GameMatrix_WIDTH * GameMatrix_HEIGHT];
Byte GameMatrix_destroyedGemNumber;
struct Vector2D currentLine[5];
bool isSpecialGemSymbolSet = false;
struct Vector2D specialGemPosition;
static Byte destroyedGemsBitfield[GameMatrix_HEIGHT]; // Used to check if a gem has been destroyed or not in current phase

//void GameMatrix_clear()
//{
//    memset(matrix, 0, sizeof(matrix));
//}

//bool GameMatrix_isFree(Byte x, Byte y)
//{
//    return matrix[blockBoardX][blockBoardY] != 0;
//}

//void GameMatrix_setElement(Byte x, Byte y, Byte value)
//{
//    matrix[blockBoardX][blockBoardY] = value;
//}

Byte GameMatrix_getTopFreeY(signed char x)
{
    for(Byte rowIndex = GameMatrix_HEIGHT - 1; rowIndex != 0; -- rowIndex)
    {
        if(GameMatrix_gameMatrix[x][rowIndex] == GameMatrix_NoGemValue)
        {
            return rowIndex;
        }
    }
    return GameMatrix_NoFreeY;
}

void GameMatrix_setSpecialGemPosition(signed char x, signed char y)
{
    const Byte gemIndexToDestroy = y >= GameMatrix_HEIGHT - 1? rand() % (GEM_NUMBER - 1) : GameMatrix_gameMatrix[x][y + 1];

    specialGemPosition.x = x;
    specialGemPosition.y = y;
    isSpecialGemSymbolSet = true;

    for(Byte i = 0; i < sizeof(GemsBlock); ++ i)
    {
        GameMatrix_setGem(x, y - i, gemIndexToDestroy);
    }
}

#define saveDestroyedGems() \
    if(currentLineGemNumber >= 3) \
    { \
        Byte actuallyDestroyedGems = 0; \
        for(Byte i = 0; i < currentLineGemNumber; ++ i) \
        { \
            const Byte offset = GameMatrix_destroyedGemNumber + actuallyDestroyedGems; \
            const Byte x = currentLine[i].x; \
            const Byte y = currentLine[i].y; \
            if(!GameMatrix_isColumnSet(destroyedGemsBitfield[y], x)) \
            { \
                GameMatrix_destroyedGems[offset].x = x; \
                GameMatrix_destroyedGems[offset].y = y; \
                GameMatrix_setColumn(destroyedGemsBitfield[y], x); \
                ++ actuallyDestroyedGems; \
            } \
        } \
        GameMatrix_destroyedGemNumber += actuallyDestroyedGems; \
    }

void GameMatrix_calculateDestroyedGems()
{
    GameMatrix_destroyedGemNumber = 0;
    memset(destroyedGemsBitfield, 0, sizeof(destroyedGemsBitfield));

    if(isSpecialGemSymbolSet)
    {
        const Byte gemIndexToDestroy = GameMatrix_gameMatrix[specialGemPosition.x][specialGemPosition.y];

        for(Byte x = 0; x < GameMatrix_WIDTH; ++ x)
        {
            for(Byte y = 0; y < GameMatrix_HEIGHT; ++ y)
            {
                if(GameMatrix_gameMatrix[x][y] == gemIndexToDestroy)
                {
                    GameMatrix_destroyedGems[GameMatrix_destroyedGemNumber].x = x;
                    GameMatrix_destroyedGems[GameMatrix_destroyedGemNumber].y = y;
                    ++ GameMatrix_destroyedGemNumber;
                }
            }
        }

        isSpecialGemSymbolSet = false;

        return;
    }

    for(Byte baseColumn = 0; baseColumn < GameMatrix_WIDTH; ++ baseColumn)
    {
        for(Byte baseRow = 0; baseRow < GameMatrix_HEIGHT; ++ baseRow)
        {
            const Byte referenceGem = GameMatrix_gameMatrix[baseColumn][baseRow];

            if(referenceGem == GameMatrix_NoGemValue)
            {
                continue;
            }

            if(baseRow < GameMatrix_HEIGHT - 2)
            {
                // Top-dowm direction
                Byte currentLineGemNumber = 0;
                Byte row = baseRow;

                do
                {
                    currentLine[currentLineGemNumber].x = baseColumn;
                    currentLine[currentLineGemNumber].y = row;

                    ++ row;
                    ++ currentLineGemNumber;
                }
                while(row < GameMatrix_HEIGHT && GameMatrix_gameMatrix[baseColumn][row] == referenceGem);

                saveDestroyedGems();
            }

            if(baseColumn < GameMatrix_WIDTH - 2)
            {
                // Left-right direction
                {
                    Byte currentLineGemNumber = 0;
                    Byte column = baseColumn;

                    do
                    {
                        currentLine[currentLineGemNumber].x = column;
                        currentLine[currentLineGemNumber].y = baseRow;

                        ++ column;
                        ++ currentLineGemNumber;
                    }
                    while(column < GameMatrix_WIDTH && GameMatrix_gameMatrix[column][baseRow] == referenceGem);

                    saveDestroyedGems();
                }

                // Left-up direction
                if(baseRow >= 2)
                {
                    Byte currentLineGemNumber = 0;
                    Byte row = baseRow;
                    Byte column = baseColumn;

                    do
                    {
                        currentLine[currentLineGemNumber].x = column;
                        currentLine[currentLineGemNumber].y = row;

                        ++ column;
                        -- row;
                        ++ currentLineGemNumber;
                    }
                    while(column < GameMatrix_WIDTH &&
                          row < GameMatrix_HEIGHT &&
                          GameMatrix_gameMatrix[column][row] == referenceGem);

                    saveDestroyedGems();
                }
            }

            if(baseColumn < GameMatrix_WIDTH - 2 && baseRow < GameMatrix_HEIGHT - 2)
            {
                // Left-down direction
                Byte currentLineGemNumber = 0;
                Byte row = baseRow;
                Byte column = baseColumn;

                do
                {
                    currentLine[currentLineGemNumber].x = column;
                    currentLine[currentLineGemNumber].y = row;

                    ++ column;
                    ++ row;
                    ++ currentLineGemNumber;
                }
                while(column < GameMatrix_WIDTH &&
                      row < GameMatrix_HEIGHT &&
                      GameMatrix_gameMatrix[column][row] == referenceGem);

                saveDestroyedGems();
            }
        }
    }
}

void GameMatrix_removeDestroyedGems()
{
    Byte modifiedColumns = 0;

    for(Byte i = 0; i < GameMatrix_destroyedGemNumber; ++ i)
    {
        const struct Vector2D* position = &GameMatrix_destroyedGems[i];
        GameMatrix_gameMatrix[position->x][position->y] = GameMatrix_NoGemValue;
        GameMatrix_setColumn(modifiedColumns, position->x);
    }

    for(Byte i = 0; i < GameMatrix_WIDTH; ++ i)
    {
        if(GameMatrix_isColumnSet(modifiedColumns, i))
        {
            AU_compactReverseArray(GameMatrix_gameMatrix[i], GameMatrix_HEIGHT, GameMatrix_NoGemValue);
        }
    }
}

bool GameMatrix_isGameOver()
{
    return GameMatrix_gameMatrix[2][0] != GameMatrix_NoGemValue;
}
