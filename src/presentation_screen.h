#ifndef PRESENTATION_SCREEN_H
#define PRESENTATION_SCREEN_H

void enterPresentationState();
void updatePresentationState();

#endif // PRESENTATION_SCREEN_H
