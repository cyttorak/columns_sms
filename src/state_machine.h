#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include "types.h"

#define SM_INVALID_STATE (Byte)(-1)

typedef void (SM_Call)();

struct SM_State
{
    SM_Call* enter;
    SM_Call* update;
    SM_Call* leave;
};

void SM_initState(struct SM_State* state, SM_Call* enter, SM_Call* update, SM_Call* leave);

struct SM_StateMachine
{
    struct SM_State* states;
    Byte stateNumber;
    Byte currentState;
};

void SM_init(struct SM_StateMachine* stateMachine, struct SM_State* states, Byte stateNumber);
void SM_setState(struct SM_StateMachine* stateMachine, Byte stateId);
void SM_update(struct SM_StateMachine* stateMachine);

#endif // STATE_MACHINE_H
