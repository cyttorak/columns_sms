#ifndef GEMS_BLOCK_H
#define GEMS_BLOCK_H

#include "types.h"

#define GemsBlock_StartX 2

#define GemsBlock_BlockSize 3
typedef Byte GemsBlock[GemsBlock_BlockSize];

// If delayOrOffset is negative gems block waits so many positive frames before moving. Otherwise each frame provided delay is added to vertical position of gems block
void GemsBlock_setStepDelay(signed char delayOrOffset);
void GemsBlock_setGemsBlock(const GemsBlock gemsBlock);
void GemsBlock_setPosition(signed char x, signed char y);
bool GemsBlock_update();
Byte GemsBlock_draw();

#endif // GEMS_BLOCK_H
