cd .\sound

vgm2psg.exe sfx_001_touch.vgm sfx_touch.psg 3
vgm2psg.exe sfx_002_L1.vgm sfx_block_destroy_1.psg 2
vgm2psg.exe sfx_002_L2.vgm sfx_block_destroy_2.psg 2
vgm2psg.exe sfx_002_L3.vgm sfx_block_destroy_3.psg 2
vgm2psg.exe sfx_002_L4.vgm sfx_block_destroy_4.psg 2
vgm2psg.exe sfx_002_L5.vgm sfx_block_destroy_5.psg 2
vgm2psg.exe sfx_002_L6.vgm sfx_block_destroy_6.psg 2
vgm2psg.exe sfx_002_L7.vgm sfx_block_destroy_7.psg 2
vgm2psg.exe sfx_002_L8.vgm sfx_block_destroy_8.psg 2
vgm2psg.exe sfx_003_select.vgm sfx_select.psg 3
vgm2psg.exe sfx_004_rearrangeblock.vgm sfx_rotate_block.psg 3

vgm2psg.exe song_001_title_screen.vgm song_001_title_screen.temp
psgcomp.exe song_001_title_screen.temp title_screen.psg

vgm2psg.exe song_002_intro_motif.vgm song_002_intro_motif.temp
psgcomp.exe song_002_intro_motif.temp intro.psg

vgm2psg.exe song_003_level.vgm song_003_level.temp
psgcomp.exe song_003_level.temp bgm1.psg

vgm2psg.exe song_004_level_alt.vgm song_004_level_alt.temp
psgcomp.exe song_004_level_alt.temp bgm2.psg

vgm2psg.exe song_00a_game_over.vgm song_00a_game_over.temp
psgcomp.exe song_00a_game_over.temp game_over.psg

del ..\bank3\*.* /Q
copy *.psg ..\bank3

cd ..

